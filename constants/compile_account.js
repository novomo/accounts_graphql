const { findByValueOfObject } = require("../../../node_normalization/filters");
module.exports = async (mysqlDb, accountId, account, balances) => {
  let a, b;

  if (!account) {
    const accountTable = await mysqlDb.getTable("accounts");
    a = await accountTable
      .select()
      .where("accountID = :accountID")
      .bind("accountID", accountId)
      .execute();

    a = a.fetchOne();
  } else {
    a = account;
  }

  if (!balances) {
    console.log(accountId, account, balances);
    const balanceTable = await mysqlDb.getTable("balances");
    b = await balanceTable
      .select()
      .where("accountID = :accountID")
      .bind("accountID", accountId)
      .execute();

    b = b.fetchAll();

    console.log(b);
  } else {
    b = balances;
  }

  return {
    accountID: a[0],
    userID: a[1],
    name: a[2],
    website: a[3],
    enabled: a[4],
    minWithdrawal: a[5],
    accountType: a[6],
    commission: a[7],
    balances: b.map((balance) => {
      return {
        balanceID: balance[0],
        accountID: balance[2],
        currency: balance[3],
        amount: balance[4],
        conversion: balance[5],
        currencyType: balance[6],
      };
    }),
  };
};
