const { findByValueOfObject } = require("../../../node_normalization/filters");
module.exports = async (mysqlDb, userId, accounts, balances) => {
  let as, bs;
  if (!accounts) {
    const accountTable = await mysqlDb.getTable("accounts");
    as = await accountTable
      .select()
      .where("userID = :userID")
      .bind("userID", userId)
      .execute();

    as = as.fetchAll();
  } else {
    as = accounts;
  }

  if (!balances) {
    const balanceTable = await mysqlDb.getTable("balances");
    let bs = await balanceTable
      .select()
      .where("userID = :userID")
      .bind("userID", currentUser)
      .execute();

    bs = bs.fetchAll();
  } else {
    bs = balances;
  }

  return accounts.map((account) => {
    const account_balances = bs.filter((balance) => {
      return balance[2] === account[0];
    });
    return {
      accountID: account[0],
      userID: account[1],
      name: account[2],
      website: account[3],
      enabled: account[4],
      minWithdrawal: account[5],
      accountType: account[6],
      commission: account[7],
      balances: account_balances.map((balance) => {
        return {
          balanceID: balance[0],
          accountID: balance[2],
          currency: balance[3],
          amount: balance[4],
          conversion: balance[5],
          currencyType: balance[6],
        };
      }),
    };
  });
};
