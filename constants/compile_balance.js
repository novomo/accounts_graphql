const { findByValueOfObject } = require("../../../node_normalization/filters");
module.exports = async (mysqlDb, balanceId, balance) => {
  let b;

  if (!balance) {
    const balanceTable = await mysqlDb.getTable("balances");
    let b = await balanceTable
      .select()
      .where("balanceID = :balanceID")
      .bind("balanceID", balanceId)
      .execute();

    b = b.fetchAll();
  } else {
    b = balance;
  }

  return {
    balanceID: b[0],
    accountID: b[2],
    currency: b[3],
    amount: b[4],
    conversion: b[5],
    currencyType: b[6],
  };
};
