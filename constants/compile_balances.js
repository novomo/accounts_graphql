const { findByValueOfObject } = require("../../../node_normalization/filters");
module.exports = async (mysqlDb, accountId, balances) => {
  let b;

  if (!balances) {
    const balanceTable = await mysqlDb.getTable("balances");
    let b = await balanceTable
      .select()
      .where("accountID = :accountID")
      .bind("accountID", accountId)
      .execute();

    b = b.fetchAll();
  } else {
    b = balances;
  }

  return b.map((balance) => {
    return {
      balanceID: balance[0],
      accountID: balance[2],
      currency: balance[3],
      amount: balance[4],
      conversion: balance[5],
      currencyType: balance[6],
    };
  });
};
