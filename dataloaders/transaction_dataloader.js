const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

module.exports.getHorseById = async (id) => {
  let session;
  try {
    session = await pool.getConnection();
  } catch (err) {
    client = await reconnect();
    session = await pool.getConnection();
  }

  const [result] = await session.query(
    `SELECT * FROM transactions where transactionID = ${id};`
  );

  transaction = result[0];
  await session.close();
  return transaction;
};

module.exports.getHorseByIds = async (ids) => {
  return ids.map((id) => getHorseById(id));
};
