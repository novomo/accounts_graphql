/*
    Account resolvers
*/
// resolver functions
const updateAccount = require("./resolvers/update_account");
const updateBalance = require("./resolvers/update_balance");
const getAccounts = require("./resolvers/get_accounts");

module.exports = {
  Mutation: {
    updateAccount,
    updateBalance,
  },
  Query: {
    getAccounts,
  },
};
