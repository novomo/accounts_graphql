/*
    Update Account
*/
// data models
const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");

const accountCompiler = require("../constants/compile_account");
// user define modules
const queryBuilder = require("../../../data/mysql/query_builder");
module.exports = async (_, { inputBalance }, { currentUser }) => {
  // check user
  ////console.log(check)
  if (!currentUser) {
    throw new Error("Not authenicated!");
  }
  try {
    //console.log(inputAccount);
    let balanceID, balance;
    //console.log(query);

    let session;
    try {
      session = await pool.getConnection();
    } catch (err) {
      client = await reconnect();
      session = await pool.getConnection();
    }
    let [balances] = await session.query(
      `SELECT * FROM balances where userID = ${currentUser} AND accountID =  ${inputBalance.accountID};`
    );
    balance = balances[0];
    if (!balance) {
      balanceID = await queryBuilder(
        session,
        "insert",
        { ...inputBalance },
        "balances"
      );
      [balances] = await session.query(
        `SELECT * FROM balances where userID = ${currentUser} AND accountID =  ${inputBalance.accountID};`
      );
      balance = balances[0];
    } else {
      balance = await queryBuilder(
        session,
        "update",
        inputBalance,
        null,
        "balanceID",
        balance[0]
      );
    }

    // Find the document in collection and update if not there add
    account = await accountCompiler(mysqlDb, balance[2], account, null);
    // find new account add to tracker via sheet queue
    //console.log(account.lastErrorObject.updatedExisting);

    ////console.log(account)
    ////console.log(account.value._doc)
    // if new taaks send info the sheet queue

    ////console.log(inputAccount)
    ////console.log(accountData)
    ////console.log({projects: projects, tasks: tasks})
    let accountsCache = await getFromCache("accounts", currentUser);

    accountsCache = accountsCache.map(
      (obj) => account.find((o) => o.accountID === obj.accountID) || obj
    );

    await updateCache("accounts", currentUser, accountsCache);
    await session.close();
    return account;
  } catch (err) {
    console.log(err);

    throw new Error("Error adding account to user!");
  }
};
