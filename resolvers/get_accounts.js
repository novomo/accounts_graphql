/* 
    Get accounts resolver
*/

// node modules

// data Models
const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const accountCompiler = require("../constants/compile_accounts");
const updateCache = require("../../../data/cacher/update_cache");
const getFromCache = require("../../../data/cacher/get_from_cache");

module.exports = async (_, { query }, { currentUser }) => {
  // check user
  ////console.log(currentUser)
  ////console.log(check)
  if (!currentUser) {
    throw new Error("Not authenicated!");
  }
  try {
    // convert query to object
    ////console.log(query)

    let accounts = await getFromCache("accounts", currentUser);

    if (accounts && Object.keys(accounts).length !== 0) {
      return accounts;
    }
    try {
      session = await pool.getConnection();
    } catch (err) {
      client = await reconnect();
      session = await pool.getConnection();
    }

    [accounts] = await session.query(
      `SELECT * FROM accounts where userID = ${currentUser};`
    );

    console.log(balances);
    let [balances] = await session.query(
      `SELECT * FROM balances where userID = ${currentUser};`
    );

    accounts = await accountCompiler(mysqlDb, currentUser, accounts, balances);
    console.log(accounts);

    await updateCache("accounts", currentUser, accounts);
    await session.close();
    return accounts;
  } catch (err) {
    console.log(err);

    throw new Error("Error obtaining accounts for user!");
  }
};
