/*
    Update Account
*/
// data models
const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const { findByValueOfObject } = require("../../../node_normalization/filters");
const accountCompiler = require("../constants/compile_account");
const balancesCompiler = require("../constants/compile_balances");
const balanceCompiler = require("../constants/compile_balance");
const getFromCache = require("../../../data/cacher/get_from_cache");
const updateCache = require("../../../data/cacher/update_cache");
// user define modules
const queryBuilder = require("../../../data/mysql/query_builder");
module.exports = async (_, { inputAccount }, { currentUser }) => {
  // check user
  ////console.log(check)
  if (!currentUser) {
    throw new Error("Not authenicated!");
  }
  try {
    //console.log(inputAccount);
    let account, accountID, balanceID, balance;
    //console.log(query);

    let session;
    try {
      session = await client.getConnection();
    } catch (err) {
      client = await reconnect();
      session = await client.getConnection();
    }

    let [accounts] = await session.query(
      `SELECT * FROM accounts where userID = ${currentUser} AND name = '${inputAccount.name}';`
    );
    account = accounts[0];
    console.log(account);
    console.log(account[0]);

    let { balances, ...sentAccount } = inputAccount;

    console.log(balances, sentAccount);
    if (!account) {
      accountID = await queryBuilder(
        session,
        "insert",
        { ...sentAccount, userID: currentUser },
        "accounts"
      );

      if (balances && balances.length > 0) {
        balances = [];
        for (let i = 0; i < balances.length; i++) {
          balanceID = await queryBuilder(
            session,
            "insert",
            { ...balances[i], userID: currentUser, accountID: accountID },
            "balances"
          );
          balances.push(await balanceCompiler(mysqlDb, balanceID, null));
        }
      }
    } else {
      accountID = await queryBuilder(
        accountTable,
        "update",
        { ...sentAccount, userID: currentUser },
        null,
        "accountID",
        account[0]
      );
      console.log(accountID);
      accountID = account[0];
      if (balances && balances.length > 0) {
        [balances] = await session.query(
          `SELECT * FROM balances where userID = ${currentUser} AND accountID = ${accountID};`
        );
        console.log(balances);
        balances = await balancesCompiler(mysqlDb, accountID, balances);
        for (let i = 0; i < balances.length; i++) {
          const saved_balance = findByValueOfObject(
            "currency",
            balances[i].currency,
            balances
          );
          console.log(saved_balance);
          if (saved_balance.length === 0) {
            balanceID = await queryBuilder(
              session,
              "insert",
              { ...balances[i], userID: currentUser, accountID: accountID },
              "balances"
            );
          } else {
            console.log("update");
            balanceID = await queryBuilder(
              session,
              "update",
              { ...balances[i], userID: currentUser, accountID: accountID },
              null,
              "balanceID",
              saved_balance[0].balanceID
            );
          }
        }
      }
    }

    // Find the document in collection and update if not there add
    account = await accountCompiler(mysqlDb, accountID, account, null);
    // find new account add to tracker via sheet queue
    console.log(account);

    ////console.log(account)
    ////console.log(account.value._doc)
    // if new taaks send info the sheet queue

    ////console.log(inputAccount)
    ////console.log(accountData)
    ////console.log({projects: projects, tasks: tasks})
    let accountsCache = await getFromCache("accounts", currentUser);

    const i = accountsCache.findIndex((x) => x.accountID === account.accountID);
    if (i === -1) {
      accountsCache.push(account);
    } else {
      accountsCache[i] = account;
    }

    await updateCache("accounts", currentUser, accountsCache);
    await session.close();
    return account;
  } catch (err) {
    console.log(err);

    throw new Error("Error adding account to user!");
  }
};
